// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef IO_HPP
#define IO_HPP

#include <string>

namespace io
{
void init();

std::string read_text_file(const std::string& path);

void print(const std::string& str);

void newline();

void print_no_newline(const std::string& str);

std::string get_input();

}  // namespace io

#endif  // IO_HPP
