// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef MAIN_MENU_HPP
#define MAIN_MENU_HPP

#include "state.hpp"

class MainMenuState : public State
{
        void on_start() override;
        void on_resume() override;
        void update() override;
};

#endif  // MAIN_MENU_HPP
