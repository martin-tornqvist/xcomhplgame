// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef PARSE_EVENT_DATA_HPP
#define PARSE_EVENT_DATA_HPP

#include <string>
#include <vector>

namespace event_data
{
struct Choice
{
        std::string text;
        std::string prereq_str;
        std::string effect_str;
};

struct Event
{
        std::string text;
        std::string prereq_str;

        std::vector<Choice> choices;
};

extern std::vector<event_data::Event> g_events;

void parse_event_data();

}  // namespace event_data

#endif  // PARSE_EVENT_DATA_HPP
