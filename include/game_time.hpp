// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GAME_TIME_HPP
#define GAME_TIME_HPP

#include <string>

namespace game_time
{
void init();

void incr_month();

std::string date_str();

}  // namespace game_time

#endif  // GAME_TIME_HPP
