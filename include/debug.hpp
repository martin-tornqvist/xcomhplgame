// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef DEBUG_HPP
#define DEBUG_HPP

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"
#include <cassert>

// TODO: Maybe use a custom assert function
#define ASSERT assert

#ifdef NDEBUG  // Release mode
#define PANIC exit(EXIT_FAILURE)
#else  // Debug mode
#define PANIC ASSERT(false)
#endif  // NDEBUG

#define TRACE_FMT(fmt, ...) \
        { \
                spdlog::info( \
                        "{}, {}(), {}: " fmt, \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__, \
                        __VA_ARGS__); \
        }

#define ERROR_FMT(fmt, ...) \
        { \
                spdlog::error( \
                        "{}, {}(), {}:" fmt, \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__, \
                        __VA_ARGS__); \
        }

#define TRACE(v) \
        { \
                spdlog::info( \
                        "{}, {}(), {}: {}", \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__, \
                        v); \
        }

#define ERROR(v) \
        { \
                spdlog::error( \
                        "{}, {}(), {}: {}", \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__, \
                        v); \
        }

#define TRACE_FN_BEGIN \
        { \
                spdlog::info( \
                        "{}, {}(), {}: [CALLED]", \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__); \
        }

#define TRACE_FN_END \
        { \
                spdlog::info( \
                        "{}, {}(), {}: [RETURNING]", \
                        __FILE__, \
                        __FUNCTION__, \
                        __LINE__); \
        }

namespace debug
{
void setup_logger();

}  // namespace debug

#endif  // DEBUG_HPP
