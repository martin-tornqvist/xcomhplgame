// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GAME_HPP
#define GAME_HPP

#include "state.hpp"

class GameState : public State
{
public:
        void on_pushed() override;
        void on_start() override;
        void update() override;
};

#endif  // GAME_HPP
