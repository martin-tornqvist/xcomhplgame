@ Nothing
? Nothing 1
? Nothing 2
? Nothing 3

@ Search
? Find spoon
> +SPOON
? Find fork
> +FORK

@ Eat spaghetti
? Eat with hands
? Eat with fork
$ FORK

@ Eat ice cream
$ SPOON
? Okay.
> +EAT_ICE_CREAM
? No!

@ Eat big dinner
$ FORK, SPOON
? Yum!

@ Win game
$ EAT_ICE_CREAM
? Alright then.
> +WIN
