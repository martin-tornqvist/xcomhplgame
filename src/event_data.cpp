// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "event_data.hpp"

#include <algorithm>
#include <cstddef>
#include <fstream>

#include "debug.hpp"
#include "text_format.hpp"

// -----------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------
static std::vector<std::string> s_event_lines;
static size_t s_event_lines_idx;

static void log_parsed_event_data()
{
        TRACE("Parsed event data:");

        for (const event_data::Event& event : event_data::g_events) {
                TRACE_FMT("@ {}", event.text);
                TRACE_FMT("$ {}", event.prereq_str);

                for (const event_data::Choice& choice : event.choices) {
                        TRACE_FMT("    ? {}", choice.text);
                        TRACE_FMT("    $ {}", choice.prereq_str);
                        TRACE_FMT("    > {}", choice.effect_str);
                }
        }
}

static void check_starts_with_sym(const std::string& str, const char c)
{
        if (str[0] != c) {
                TRACE_FMT("Expected line '{}' to start with symbol '{}'", str, c);

                log_parsed_event_data();

                PANIC;
        }
}

static void read_event_files()
{
        s_event_lines.clear();

        std::ifstream f("data/events/events.txt");

        std::string line;

        while (std::getline(f, line)) {
                text_format::trim(line);

                if (line.empty()) {
                        continue;
                }

                const char sym = line[0];

                if (
                        (sym != '@') &&
                        (sym != '?') &&
                        (sym != '$') &&
                        (sym != '>')) {
                        continue;
                }

                s_event_lines.push_back(line);
        }
}

static std::string get_current_line()
{
        if (s_event_lines_idx >= s_event_lines.size()) {
                return "";
        }

        return s_event_lines[s_event_lines_idx];
}

static std::string go_to_next_line()
{
        ++s_event_lines_idx;

        return get_current_line();
}

static std::string strip_first_symbol(const std::string& line)
{
        return text_format::trim_copy(line.substr(1));
}

static event_data::Choice parse_choice()
{
        std::string line = s_event_lines[s_event_lines_idx];

        check_starts_with_sym(line, '?');

        event_data::Choice choice;
        choice.text = strip_first_symbol(line);

        line = go_to_next_line();

        if (line.empty()) {
                return choice;
        }

        if (line[0] == '$') {
                choice.prereq_str = strip_first_symbol(line);

                line = go_to_next_line();
        }

        if (line.empty()) {
                return choice;
        }

        if (line[0] == '>') {
                choice.effect_str = strip_first_symbol(line);

                go_to_next_line();
        }

        return choice;
}

static void parse_event()
{
        std::string line = s_event_lines[s_event_lines_idx];

        check_starts_with_sym(line, '@');

        event_data::Event event;
        event.text = strip_first_symbol(line);

        line = go_to_next_line();

        if (line[0] == '$') {
                event.prereq_str = strip_first_symbol(line);

                line = go_to_next_line();
        }

        while (true) {
                event_data::Choice choice = parse_choice();

                event.choices.push_back(choice);

                line = get_current_line();

                if (line.empty() || (line[0] != '?')) {
                        break;
                }
        }

        event_data::g_events.push_back(event);
}

static void parse_all_events()
{
        s_event_lines_idx = 0U;

        std::string line;

        while (true) {
                parse_event();

                line = get_current_line();

                if (line.empty()) {
                        return;
                }
        }
}

// -----------------------------------------------------------------------------
// event_data
//-----------------------------------------------------------------------------
namespace event_data
{
std::vector<event_data::Event> g_events;

void parse_event_data()
{
        s_event_lines.clear();

        read_event_files();

        parse_all_events();

        log_parsed_event_data();
}

}  // namespace event_data
