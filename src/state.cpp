// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "state.hpp"

#include "debug.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static std::vector<std::unique_ptr<State>> s_current_states;

static void run_state_iteration()
{
        states::start();

        if (states::is_empty()) {
                return;
        }

        states::update();

        // if (!io::is_window_open())
        // {
        //         states::pop_all();
        // }
}

//-----------------------------------------------------------------------------
// states
//-----------------------------------------------------------------------------
namespace states
{
void init()
{
        cleanup();
}

void cleanup()
{
        s_current_states.resize(0);
}

void run()
{
        while (!is_empty()) {
                run_state_iteration();
        }
}

void run_until_state_done(std::unique_ptr<State> state)
{
        auto* state_addr = state.get();

        push(std::move(state));

        while (contains_state(state_addr)) {
                run_state_iteration();
        }
}

void start()
{
        while (!is_empty() && !s_current_states.back()->has_started()) {
                State* const state = s_current_states.back().get();

                state->set_started();

                // NOTE: This may cause states to be pushed/popped - be careful
                // about using the "state" pointer beyond this call!
                state->on_start();
        }
}

void update()
{
        if (is_empty()) {
                return;
        }

        s_current_states.back()->update();
}

void push(std::unique_ptr<State> state)
{
        // Pause the current state
        if (!is_empty()) {
                s_current_states.back()->on_paused();
        }

        s_current_states.push_back(std::move(state));

        s_current_states.back()->on_pushed();

        // io::flush_input();
}

void pop()
{
        if (is_empty()) {
                return;
        }

        s_current_states.back()->on_popped();

        s_current_states.pop_back();

        if (!is_empty()) {
                s_current_states.back()->on_resume();
        }

        // io::flush_input();
}

void pop_all()
{
        while (!is_empty()) {
                s_current_states.back()->on_popped();

                s_current_states.pop_back();
        }
}

// bool contains_state(const StateId id)
// {
//         for (auto& state : s_current_states)
//         {
//                 if (state->id() == id)
//                 {
//                         return true;
//                 }
//         }

//         return false;
// }

bool contains_state(const State* const state)
{
        return std::any_of(
                std::begin(s_current_states),
                std::end(s_current_states),
                [state](const auto& s) { return s.get() == state; });
}

// void pop_until(const StateId id)
// {
//         if (is_empty() || !contains_state(id))
//         {
//                 ASSERT(false);

//                 return;
//         }

//         while (s_current_states.back().get()->id() != id)
//         {
//                 pop();
//         }
// }

bool is_current_state(const State* const state)
{
        if (is_empty()) {
                return false;
        }

        return state == s_current_states.back().get();
}

bool is_empty()
{
        return s_current_states.empty();
}

}  // namespace states
