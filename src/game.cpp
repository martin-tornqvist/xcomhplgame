// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "game.hpp"

#include <cstddef>
#include <fmt/format.h>
#include <memory>
#include <string>
#include <vector>

#include "debug.hpp"
#include "event_data.hpp"
#include "io.hpp"
#include "random.hpp"
#include "state.hpp"
#include "text_format.hpp"

// -----------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------
std::vector<std::string> s_tags;

static std::string strip_first_symbol(const std::string& line)
{
        return text_format::trim_copy(line.substr(1));
}

static bool has_tag(const std::string& tag)
{
        return (
                std::find(
                        std::cbegin(s_tags),
                        std::cend(s_tags),
                        text_format::to_upper(tag)) !=
                std::cend(s_tags));
}

static void remove_tag(const std::string& tag)
{
        s_tags.erase(
                std::remove(
                        std::begin(s_tags),
                        std::end(s_tags),
                        text_format::to_upper(tag)),
                std::end(s_tags));
}

static void add_tag(const std::string& tag)
{
        if (!has_tag(tag)) {
                s_tags.push_back(text_format::to_upper(tag));
        }
}

static bool is_prereqs_fulfilled(const std::string& prereq_str)
{
        const std::vector<std::string> tokens = text_format::split_by_delim(prereq_str, ',');

        for (std::string token : tokens) {
                text_format::trim(token);

                std::string tag = token;

                bool is_negative = false;

                if (token[0] == '!') {
                        tag = strip_first_symbol(token);

                        is_negative = true;
                }

                const bool tag_found = has_tag(tag);

                if (!is_negative && !tag_found) {
                        return false;
                }

                if (is_negative && tag_found) {
                        return false;
                }
        }

        return true;
}

static std::vector<event_data::Choice> get_allowed_choices(const event_data::Event& event)
{
        std::vector<event_data::Choice> allowed_choices;

        std::copy_if(
                std::cbegin(event.choices),
                std::cend(event.choices),
                std::back_inserter(allowed_choices),
                [&](const event_data::Choice& c) {
                        return is_prereqs_fulfilled(c.prereq_str);
                });

        return allowed_choices;
}

static bool is_event_allowed(const event_data::Event& event)
{
        return is_prereqs_fulfilled(event.prereq_str);
}

static event_data::Event get_random_event()
{
        std::vector<event_data::Event> event_bucket;

        for (const event_data::Event& event : event_data::g_events) {
                if (is_event_allowed(event) && !get_allowed_choices(event).empty()) {
                        event_bucket.push_back(event);
                }
        }

        return rnd::element(event_bucket);
}

static void handle_choice_effect(const std::string& effect_str)
{
        TRACE(effect_str);

        if (effect_str.empty()) {
                return;
        }

        const std::vector<std::string> tokens =
                text_format::split_by_delim(effect_str, ',');

        for (std::string token : tokens) {
                text_format::trim(token);

                // TODO: Handle random chance syntax.

                const char sym = token[0];

                ASSERT((sym == '+') || (sym == '-'));

                const std::string tag = strip_first_symbol(token);

                if (sym == '+') {
                        add_tag(tag);
                }
                else {
                        remove_tag(tag);
                }
        }
}

// -----------------------------------------------------------------------------
// GameState
// -----------------------------------------------------------------------------
void GameState::on_pushed()
{
        // game_time::init();

        s_tags.clear();

        event_data::parse_event_data();

        // s_locations.clear();

        // location::LocationData loc = location::get_location_data("STOCKHOLM");
        // s_locations.push_back(loc);

        rnd::seed();
}

void GameState::on_start()
{
        // const auto str = io::read_text_file("intro.txt");
}

void GameState::update()
{
        io::print("");
        io::print("==================================================");
        io::print("");

        event_data::Event event = get_random_event();

        io::print(event.text);

        const std::vector<event_data::Choice> choices = get_allowed_choices(event);

        for (size_t i = 0; i < choices.size(); ++i) {
                io::print(fmt::format("{}: {}", i + 1, choices[i].text));
        }

        io::print("d: Debug.");
        io::print("q: Quit.");

        const std::string input = io::get_input();
        io::newline();

        if (input == "d") {
                std::string tags_str;

                for (const std::string& tag : s_tags) {
                        text_format::append_as_comma_list(tags_str, tag);
                }

                if (tags_str.empty()) {
                        tags_str = "<no tags>";
                }

                io::print(fmt::format("TAGS: {}", tags_str));

                return;
        }
        else if (input == "q") {
                states::pop();

                return;
        }

        const int choice_idx = std::stoi(input) - 1;
        const int nr_choices = static_cast<int>(choices.size());

        TRACE_FMT("choice index: {}", choice_idx);

        if ((choice_idx >= 0) && (choice_idx < nr_choices)) {
                const event_data::Choice& choice = choices[static_cast<size_t>(choice_idx)];

                handle_choice_effect(choice.effect_str);
        }

        if (has_tag("WIN")) {
                io::print("");
                io::print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                io::print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                io::print("Congratulations, you have won!");
                io::print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                io::print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                io::print("");

                states::pop();

                return;
        }
}
