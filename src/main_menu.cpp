// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "main_menu.hpp"

#include <fmt/core.h>
#include <memory>
#include <vector>

#include "game.hpp"
#include "io.hpp"
#include "state.hpp"

void MainMenuState::on_start()
{
        // add_menu_opt({"n", "New game"});
        // add_menu_opt({"q", "Quit"});
}

void MainMenuState::on_resume()
{
}

void MainMenuState::update()
{
        io::print("n: New game");
        io::print("q: Quit");

        const std::string input = io::get_input();
        io::newline();

        if (input == "n") {
                auto game_state = std::make_unique<GameState>();
                states::push(std::move(game_state));
        }
        else if (input == "q") {
                states::pop_all();
        }
}
