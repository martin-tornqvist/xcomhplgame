// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include <memory>
#include <utility>

#include "debug.hpp"
#include "io.hpp"
#include "main_menu.hpp"
#include "state.hpp"

int main(int argc, char** argv)
{
        (void)argc;
        (void)argv;

        debug::setup_logger();

        io::init();

        io::newline();

        auto main_menu = std::make_unique<MainMenuState>();

        states::push(std::move(main_menu));

        states::run();
}
