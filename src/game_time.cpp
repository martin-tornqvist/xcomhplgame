// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "game_time.hpp"

#include "fmt/core.h"
#include <vector>

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static int s_year = 0;
static size_t s_month_idx = 0U;

static const std::vector<std::string> s_months {
        "January",
        "February",
        "Mars",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"};

// -----------------------------------------------------------------------------
// game_time
// -----------------------------------------------------------------------------
namespace game_time
{
void init()
{
        s_year = 1664;
        s_month_idx = 0;
}

void incr_month()
{
        if (s_month_idx == (s_months.size() - 1U))
        {
                s_month_idx = 0U;
                ++s_year;
        }
        else
        {
                ++s_month_idx;
        }
}

std::string date_str()
{
        return fmt::format("{}, {}", s_year, s_months[s_month_idx]);
}

}  // namespace game_time
