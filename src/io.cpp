// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "io.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

#include "debug.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// io
// -----------------------------------------------------------------------------
namespace io
{
void init()
{
}

std::string read_text_file(const std::string& path)
{
        std::ifstream infile {path};

        if (!infile) {
                ERROR_FMT("Could not find/read file at: '{}'", path);
                PANIC;
        }

        return {
                std::istreambuf_iterator<char>(infile),
                std::istreambuf_iterator<char>()};
}

void print(const std::string& str)
{
        print_no_newline(str);
        std::cout << std::endl;
}

void newline()
{
        std::cout << std::endl;
}

void print_no_newline(const std::string& str)
{
        std::cout << str;
}

std::string get_input()
{
        std::string input;

        std::cin >> input;

        return input;
}

}  // namespace io
