// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "debug.hpp"

namespace debug
{
void setup_logger()
{
        // TODO: File is not written if game crashes. How to fix?

        auto file_logger =
                spdlog::basic_logger_mt(
                        "xcom-hpl-game",
                        "game.log",
                        true);  // Overwrite log

        spdlog::set_default_logger(file_logger);
}

}  // namespace debug
